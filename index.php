<?php

include ('Animal.php');
$sheep = new Animal("shaun");

echo $sheep->get_name(); 
echo $sheep->get_legs(); // 4
echo $sheep->get_cold_blooded();

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>